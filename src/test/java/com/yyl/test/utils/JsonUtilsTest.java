package com.yyl.test.utils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.ibm.icu.text.SimpleDateFormat;
import com.yyl.common.utils.json.JsonUtils_Jackson;


public class JsonUtilsTest {
	@Test
	public void testJson2Object() throws Exception {
		
		//测试对象转json
		ExampleBean bean = new ExampleBean();
		LinkBean link = new LinkBean();
		link.setId(10);

		bean.setId(1);
		bean.setGender("男");
		bean.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse("2015-12-30"));
		bean.setScore(44.44);
		bean.setName("yyl");
		bean.setReal(false);
		bean.setLink(link);
		String result = JsonUtils_Jackson.bean2Json(bean);
		System.out.println(result);
		
		System.out.println("--------------------------------------");
		//测试json转对象
		ExampleBean bean1 = JsonUtils_Jackson.json2Bean(result, ExampleBean.class);
		System.out.println(bean1.toString());
		System.out.println("--------------------------------------");
		//测试list<map>转json
		List<Map<String,String>> lists = new ArrayList<Map<String,String>>();
		Map<String,String> maps = new HashMap<String, String>();
		Map<String,String> maps2 = new HashMap<String, String>();
		maps.put("姓名", "yangyl");
		maps.put("性别", "男"); 
		lists.add(maps);
		maps2.put("姓名", "yangyl2");
		maps2.put("性别", "男2");
		lists.add(maps2);
		String result1 = JsonUtils_Jackson.bean2Json(lists);
		System.out.println(result1);
	}
}
