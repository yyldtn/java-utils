package com.yyl.test.utils;

import java.util.Date;
import java.util.List;

public class ExampleBean {
	
	private int id;
	
	private String name;
	
	private boolean isReal;
	
	public boolean isReal() {
		return isReal;
	}

	public void setReal(boolean isReal) {
		this.isReal = isReal;
	}

	private Date birthday;
	
	private Gender gender;
	
	private double score;
	
	private LinkBean link;
	
	private List<LinkBean> lists;
	

	public LinkBean getLink() {
		return link;
	}

	public void setLink(LinkBean link) {
		this.link = link;
	}

	public List<LinkBean> getLists() {
		return lists;
	}

	public void setLists(List<LinkBean> lists) {
		this.lists = lists;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(String gender) {
		if (gender.equals("男") || gender.equals("MALE")) {
			this.gender = Gender.MALE;
		} else {
			this.gender = Gender.FAMALE;
		}
		
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	
	
}
