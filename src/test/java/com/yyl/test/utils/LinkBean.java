package com.yyl.test.utils;

import java.util.List;
import java.util.Map;

public class LinkBean {

	private int id;
	
	private List<String> photos;
	
	private Map<String,Double> score;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getPhotos() {
		return photos;
	}

	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public Map<String, Double> getScore() {
		return score;
	}

	public void setScore(Map<String, Double> score) {
		this.score = score;
	}
	
	
}
