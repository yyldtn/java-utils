package com.yyl.test.Excel;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.yyl.common.entity.BaseException;
import com.yyl.common.utils.DateUtils;
import com.yyl.common.utils.excel.ExcelTools;
import com.yyl.common.utils.excel.FileTools;

public class TestExportExcel {

	@Test
	//测试直接导出excel文件
    public void testExportExcelDirectly() throws Exception {
         String export = ExcelTools.OFFICE_EXCEL_2010_SUFFIX;
         String fileName ="filename-"+DateUtils.formatDate(new Date(), "yyyy-MM-dd");
         String sheetName = "filename";
         
         OutputStream out = new FileOutputStream("E://a.xls"); 
         List<String> heads = new ArrayList<String>();
         heads.add("天天");
         heads.add("向上");
         
         String[][] data = new String[3][heads.size()];

         for (int j, i = 0; i < 3; i++) {
             j = 0;
             data[i][j++] = "1";
             data[i][j++] = "2";
         }
         if (export.equals(ExcelTools.OFFICE_EXCEL_2003_SUFFIX)) {
             try {
                ExcelTools.writeToXLS(heads, data, sheetName, out);
                 
             } catch (IOException ex) {
                 throw new FileTools.FileException("file write error!", FileTools.FileException.FILE_WRITE_ERROR);
             } catch (Exception ex) {
                 ex.printStackTrace();
                 throw new BaseException("upload file fail, connection error!");
             }
         } else {
             try {
                 ExcelTools.writeToXLSX(heads, data, sheetName, out);

             } catch (IOException ex) {
                 throw new FileTools.FileException("file write error!", FileTools.FileException.FILE_WRITE_ERROR);
             } catch (Exception ex) {
                 ex.printStackTrace();
                 throw new BaseException("upload file fail, connection error!");
             }
         }
         
         try {
             out.close();
         } catch (IOException ex) {
             ex.printStackTrace();
         }
        
	}
	
	@Test
	//测试文件上传到服务器，返回下载的url
	public void testExportExcelByUrl() throws Exception {
		
	}
}
